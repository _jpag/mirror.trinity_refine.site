<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package BrainBlank
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function brainblank_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'brainblank_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function brainblank_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'brainblank_pingback_header' );

/**
 * ACF Gutenberg Blocks
 */
add_action('acf/init', 'my_acf_init');

function my_acf_init() {
	
	if( function_exists('acf_register_block') ) {	
		// register a slideshow block
		acf_register_block(array(
			'name'				=> 'slideshow',
			'title'				=> __('Hero home page'),
			'description'		=> __('A custom home hero block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'brainup',
			'icon'				=> 'images-alt2',
			'keywords'			=> array( 'home', 'home' ),
		));
		// register a maps block
		// acf_register_block(array(
		// 	'name'				=> 'maps',
		// 	'title'				=> __('Maps'),
		// 	'description'		=> __('A custom maps block.'),
		// 	'render_callback'	=> 'my_acf_block_render_callback',
		// 	'category'			=> 'brainup',
		// 	'icon'				=> 'admin-site-alt',
		// 	'keywords'			=> array( 'Maps', 'Mappa' ),
		// ));
		// register a contact block
		// acf_register_block(array(
		// 	'name'				=> 'contact',
		// 	'title'				=> __('Contact'),
		// 	'description'		=> __('A custom contact block.'),
		// 	'render_callback'	=> 'my_acf_block_render_callback',
		// 	'category'			=> 'brainup',
		// 	'icon'				=> 'email',
		// 	'keywords'			=> array( 'Contact', 'Contatti e form' ),
		// ));
		// register a paragraph block
		acf_register_block(array(
			'name'				=> 'paragraph',
			'title'				=> __('Paragraph'),
			'description'		=> __('A custom paragraph block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'brainup',
			'icon'				=> 'editor-alignleft',
			'keywords'			=> array( 'Paragraph', 'Paragrafo semplice' ),
        ));
        // register a paragraph slideshow block
		acf_register_block(array(
			'name'				=> 'paragraph_slideshow',
			'title'				=> __('Paragraph with slideshow'),
			'description'		=> __('A custom paragraph block with image.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'brainup',
			'icon'				=> 'editor-alignleft',
			'keywords'			=> array( 'Paragraph with slideshow', 'Paragraph with slideshow' ),
        ));
        // register a home refine scheme block
		acf_register_block(array(
			'name'				=> 'home_refine_scheme',
			'title'				=> __('Home Refine scheme'),
			'description'		=> __('A custom paragraph block with scheme.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'brainup',
			'icon'				=> 'chart-area',
			'keywords'			=> array( 'Home Refine scheme', 'Home Refine scheme' ),
        ));
        // register a home refine news block
		acf_register_block(array(
			'name'				=> 'news',
			'title'				=> __('News and Twitter'),
			'description'		=> __('A custom news + twitter block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'brainup',
			'icon'				=> 'admin-site-alt3',
			'keywords'			=> array( 'News and Twitter', 'News and Twitter' ),
		));
		// register a home refine news block
		acf_register_block(array(
			'name'				=> 'teasers',
			'title'				=> __('Teasers'),
			'description'		=> __('A custom teasers repeater block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'brainup',
			'icon'				=> 'excerpt-view',
			'keywords'			=> array( 'Teasers', 'Teasers' ),
		));
		// register a paragraph with popup block
		acf_register_block(array(
			'name'				=> 'paragraph-fancybox',
			'title'				=> __('Paragraph with popup'),
			'description'		=> __('A custom paragraph block with image popup.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'brainup',
			'icon'				=> 'editor-expand',
			'keywords'			=> array( 'Paragraph with popup', 'Paragraph with popup' ),
		));
		// register a partners block
		acf_register_block(array(
			'name'				=> 'partners',
			'title'				=> __('Partners'),
			'description'		=> __('A custom partners repeater block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'brainup',
			'icon'				=> 'id-alt',
			'keywords'			=> array( 'Partners', 'Partners' ),
		));
		// register a paragraph with 2 columns
		acf_register_block(array(
			'name'				=> 'paragraph-columns',
			'title'				=> __('Paragraph with 2 columns'),
			'description'		=> __('A custom paragraph block with 2 columns.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'brainup',
			'icon'				=> 'editor-ul',
			'keywords'			=> array( 'Paragraph with 2 columns', 'Paragraph with 2 columns' ),
		));
		// register a image full width row 
		acf_register_block(array(
			'name'				=> 'image-row',
			'title'				=> __('Image full width row'),
			'description'		=> __('A custom row block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'brainup',
			'icon'				=> 'slides',
			'keywords'			=> array( 'Image full width row', 'Image full width row' ),
		));
		// register a paragraph + teaser
		acf_register_block(array(
			'name'				=> 'paragraph-teaser',
			'title'				=> __('Paragraph + Teaser'),
			'description'		=> __('A custom teasers + paragraph block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'brainup',
			'icon'				=> 'editor-alignleft',
			'keywords'			=> array( 'Paragraph + Teaser', 'Paragraph + Teaser' ),
		));
		// register a paragraph with 2 popup block
		acf_register_block(array(
			'name'				=> 'double-fancybox',
			'title'				=> __('Paragraph with 2 popup'),
			'description'		=> __('A custom paragraph block with 2 image popup.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'brainup',
			'icon'				=> 'editor-expand',
			'keywords'			=> array( 'Paragraph with 2 popup', 'Paragraph with 2 popup' ),
		));
		acf_update_setting('google_api_key', 'AIzaSyBrCDdwb9CpTMKo50WxZBTpr49i9LEa-gY');
	}
}
function my_acf_block_render_callback( $block ) {	
	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);

	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/template-parts/blocks/content-{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/blocks/content-{$slug}.php") );
	}
}


/**
 * Remove empty paragraphs created by wpautop()
 * @author Ryan Hamilton
 * @link https://gist.github.com/Fantikerz/5557617
 */
function remove_empty_p( $content ) {
	$content = force_balance_tags( $content );
	$content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
	$content = preg_replace( '~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content );
	return $content;
}
add_filter('the_content', 'remove_empty_p', 20, 1);

/**
 * Edit in blank target
 */
add_filter( 'edit_post_link', function( $link, $post_id, $text )
{
    // Add the target attribute 
    if( false === strpos( $link, 'target=' ) )
        $link = str_replace( '<a ', '<a target="_blank" ', $link );

    return $link;
}, 10, 3 );

/**
 * Disable REST API
 */
// add_filter( 'rest_authentication_errors', function( $result ) {
//     if ( ! empty( $result ) ) {
//         return $result;
//     }
//     if ( ! is_user_logged_in() ) {
//         return new WP_Error( 'rest_not_logged_in', 'You are not currently logged in.', array( 'status' => 401 ) );
//     }
//     return $result;
// });

/**
 * A custom ACF widget.
 */
class ACF_Custom_Widget extends WP_Widget {

    /**
    * Register widget with WordPress.
    */
    function __construct() {
        parent::__construct(
            'acf_custom_widget', // Base ID
            __('ACF Custom Widget', 'text_domain'), // Name
            array( 'description' => __( 'A custom ACF widget', 'text_domain' ), 'classname' => 'acf-custom-widget js-customWidget' ) // Args
        );
    }

    /**
    * Front-end display of widget.
    *
    * @see WP_Widget::widget()
    *
    * @param array $args     Widget arguments.
    * @param array $instance Saved values from database.
    */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        if ( !empty($instance['title']) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
        }

        echo get_field('title', 'widget_' . $args['widget_id']);

        // BEGIN ACF CODE
            echo '<div class="subtitle">'
            	. get_field('subtitle', 'widget_' . $args['widget_id']) .
			'</div>';
			echo '<div class="text">'
            	. get_field('text', 'widget_' . $args['widget_id']) .
			'</div>';

			echo '<div class="links">
				<a href="'
					. get_field('link_info', 'widget_' . $args['widget_id'])['url'] .
				'" target="'
					. get_field('link_info', 'widget_' . $args['widget_id'])['target'] .
				'">'
					. get_field('link_info', 'widget_' . $args['widget_id'])['title'] .
				'</a>
				
				<a href="'
            		. get_field('link_register', 'widget_' . $args['widget_id'])['url'] .
				'" target="'
					. get_field('link_register', 'widget_' . $args['widget_id'])['target'] .
				'">'
					. get_field('link_register', 'widget_' . $args['widget_id'])['title'] .
				'</a>
				<div class="widget_close js-widgetClose"></div>
			</div>';
        // END ACF CODE

        echo $args['after_widget'];
    }

    /**
    * Back-end widget form.
    *
    * @see WP_Widget::form()
    *
    * @param array $instance Previously saved values from database.
    */
    public function form( $instance ) {
        if ( isset($instance['title']) ) {
            $title = $instance['title'];
        }
    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    </p>
    <?php
    }

    /**
    * Sanitize widget form values as they are saved.
    *
    * @see WP_Widget::update()
    *
    * @param array $new_instance Values just sent to be saved.
    * @param array $old_instance Previously saved values from database.
    *
    * @return array Updated safe values to be saved.
    */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

        return $instance;
    }

} // class ACF_Custom_Widget

// register ACF_Custom_Widget widget
add_action( 'widgets_init', function(){
  register_widget( 'ACF_Custom_Widget' );
});

add_filter( 'body_class','partner_body_Class' );
function partner_body_Class( $classes ) {
    if ( is_page_template( 'page-partner.php' ) ) {
        $classes[] = 'partner';
    }
    return $classes;
}



/**
 * Hide editor in template
 */
function remove_editor() {
    if (isset($_GET['post'])) {
        $id = $_GET['post'];
        $template = get_post_meta($id, '_wp_page_template', true);
        switch ($template) {
            case 'page-contact.php':
            // the below removes 'editor' support for 'pages'
            // if you want to remove for posts or custom post types as well
            // add this line for posts:
            // remove_post_type_support('post', 'editor');
            // add this line for custom post types and replace 
            // custom-post-type-name with the name of post type:
            // remove_post_type_support('custom-post-type-name', 'editor');
            remove_post_type_support('page', 'editor');
            break;
            default :
            // Don't remove any other template.
            break;
        }
    }
}
add_action('init', 'remove_editor');

/**
 * Custom Gutenberg category block
 */
add_filter( 'block_categories', function( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug'  => 'brainup',
                'title' => 'BrainUp',
            ),
        )
    );
}, 10, 2 );