<?php
/**
 * BrainBlank functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package BrainBlank
 */

if ( ! function_exists( 'brainblank_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function brainblank_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on BrainBlank, use a find and replace
		 * to change 'brainblank' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'brainblank', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'brainblank' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'brainblank_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'brainblank_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function brainblank_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'brainblank_content_width', 640 );
}
add_action( 'after_setup_theme', 'brainblank_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function brainblank_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Social', 'brainblank' ),
		'id'            => 'social',
		'description'   => esc_html__( 'Add widgets here.', 'brainblank' ),
		'before_widget' => '<div id="%1$s" class="c-socialWidget">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer info', 'brainblank' ),
		'id'            => 'footerright',
		'description'   => esc_html__( 'Add widgets here.', 'brainblank' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<p class="c-footer__title">',
		'after_title'   => '</p>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer disclaimer', 'brainblank' ),
		'id'            => 'footerleft',
		'description'   => esc_html__( 'Add widgets here.', 'brainblank' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<p class="c-footer__title">',
		'after_title'   => '</p>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'brainblank' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'brainblank' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Custom area', 'brainblank' ),
		'id'            => 'customarea',
		'description'   => esc_html__( 'Add widgets here.', 'brainblank' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<div class="c-customarea">',
		'after_title'   => '</div>',
	) );
}
add_action( 'widgets_init', 'brainblank_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function brainblank_scripts() {
	wp_enqueue_style( 'brainblank-style', get_stylesheet_uri() );

	wp_enqueue_script( 'cycle', get_template_directory_uri() . '/js/plugins/jquery.cycle2.min.js', array('jquery'), '20141007', true );

	wp_enqueue_script( 'cycle_swipe', get_template_directory_uri() . '/js/plugins/jquery.cycle2.swipe.min.js', array('jquery'), '20141007', true );

	wp_enqueue_script( 'fancybox', get_template_directory_uri() . '/js/plugins/jquery.fancybox.min.js', array(), '3.5.7', true );
	
	// wp_enqueue_script( 'owl_carousel', get_template_directory_uri() . '/js/plugins/owl.carousel.min.js', array('jquery'), '2.3.4', true );

	wp_enqueue_script( 'brainblank-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'brainblank-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.0.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'brainblank_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

