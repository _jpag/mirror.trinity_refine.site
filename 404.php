<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package BrainBlank
 */

get_header();
?>
	<main id="primary" class="content-area">
		<article id="main" class="site-main">

			<section class="error-404 not-found l-container_page">
				<div class="page-content">
				<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'brainblank' ); ?></h1>
				<img src="<?php echo get_template_directory_uri() ?>/images/img-error-404.svg">
				<a href="<?php echo get_home_url() ?>" class="o-button o-button_light">HOME PAGE</a>
			
			</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</article><!-- #main -->
	</main><!-- #primary -->

<?php
get_footer();
