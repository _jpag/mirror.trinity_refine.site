<?php
/**
 * Template part for displaying page content in page.php
 * 
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BrainBlank
 */
$image = get_post_thumbnail_id($post->ID);
$size = 'medium';
$alt = array('alt'=>get_the_title());
$size_bg = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
$page_intro_text = get_field('page_intro_text');
$class = '';
if (!$page_intro_text) {
	$class = 'fullWidth';
}
?>

<article id="post-<?php the_ID(); ?>" class="c-page">
	<?php if($image): ?>
		<header class="c-page__header">
			<div class="c-pageHeader__background"  style="background-image: url(<?php echo wp_get_attachment_image_src($image, $size_bg)[0] ?>)"></div>
			<div class="c-pageHeader__inner">
				<?php the_title( '<h1 class="c-page__title">', '</h1>' ); ?>
				<?php if ($page_intro_text):?>
					<h2 class="c-page__subtitle"><?php echo $page_intro_text  ?></h2>
				<?php endif; ?>
			</div>
			<div class="c-page__thumbnail">
				<div class="u-cover-image">
					<img src="<?php echo wp_get_attachment_image_url($image, $size, false) ?>" alt="<?php echo $alt ?>">
				</div>
			</div>
			<a class="js-anchorScroll c-scrollButton" href="#content">
				<img src="<?php echo get_template_directory_uri() ?>/images/icon-scroll.svg" alt=""><span>SCROLL DOWN</span>
			</a>
		</header><!-- .entry-header with image-->
	<?php else: ?>
	<header class="c-page__simpleHeader <?php echo $class ?>">
		<div class="inner l-container">
			<?php the_title( '<h1 class="c-page__title">', '</h1>' ); ?>
			<?php if ($page_intro_text):?>
				<h2 class="c-page__subtitle"><?php echo $page_intro_text  ?></h2>
			<?php endif; ?>
		</div>
	</header><!-- .entry-header without image-->
	<?php endif; ?>

	<div id="content" class="entry-content">
		<?php
		the_content();
		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'brainblank' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'brainblank' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					false
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
