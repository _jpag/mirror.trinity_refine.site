<?php
	/**
	 * Block Name: Partner
	 */
	$partner = get_field('partner');
?>

<?php if ($partner): ?>
<section class="c-partners o-background__leftSmall">
	<div class="l-container inner">
		<?php while(has_sub_field('partner')): 
			$image = get_sub_field('image');
			$link = get_sub_field('link');
		?>
		<a href="<?php echo $link['url'] ?>" target="<?php echo $link['target'] ?>" class="c-partner">
			<div class="u-cover-image">
				<img src="<?php echo $image['sizes']['medium'] ?>" alt="<?php echo $image['alt'] ?>">
			</div>
		</a>
		<?php endwhile; ?>
	</div>
</section>
<?php endif ?>