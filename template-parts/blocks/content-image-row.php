<?php
	/**
	 * Block Name: Image row
	 */
    $image = get_field('image');
?>
<?php if($image): ?>
    <section class="c-imageFullWidth" style="background-image: url(<?php echo $image['sizes']['large'] ?>)"></section>
<?php endif; ?>