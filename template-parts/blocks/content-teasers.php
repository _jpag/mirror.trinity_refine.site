<?php
	/**
	 * Block Name: Teasers
	 */
	$teaser = get_field('teaser');
	$style = get_field('style');

	if (get_field('display') == 1)
		$display = 'block visibile';
	else
		$display = 'block invisible'; 
?>

<?php if ($teaser): ?>
<section class="c-teasers o-background__leftSmall o-background__rightSmall <?php echo $style ?> <?php echo $display ?>">
	<div class="l-container inner">
		<?php while(has_sub_field('teaser')): 
			$title = get_sub_field('title');
			$text = get_sub_field('text');
			$link = get_sub_field('link');
			$image = get_sub_field('image');
			$image_popup = get_sub_field('image_full');
			$string = preg_replace('/\s+/', '', $title);
			$id = get_sub_field('id');
		?>
		<div id="<?php echo $id ?>" class="c-teaser">
			<?php if ($title): ?>
				<h3><?php echo $title ?></h3>
			<?php endif ?>
			<?php if ($text): ?>
				<p><?php echo $text ?></p>
			<?php endif ?>
			<?php if ($image): ?>
				<a data-fancybox="<?php echo $string ?>_image" href="<?php echo $image_popup['sizes']['medium'] ?>">
					<img src="<?php echo $image['sizes']['medium'] ?>" alt="<?php echo $image['alt'] ?>">
				</a>
			<?php endif ?>

			<?php if ($link): ?>
				<a class="o-button js-scrollFromPage" target="<?php echo $link['target'] ?>" href="<?php echo $link['url'] ?>">
					<?php 
						if ($link['title']) {
							echo $link['title'];
						} else {
							echo 'more info';
						}
					?>
				</a>
			<?php endif ?>			
		</div>
		<?php endwhile; ?>
	</div>
</section>
<?php endif ?>