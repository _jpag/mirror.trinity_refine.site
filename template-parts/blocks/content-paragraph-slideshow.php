<?php
	/**
	 * Block Name: Paragraph with slideshow
	 */
	$title = get_field('title');
	$text = get_field('text');
	$link = get_field('link');
	$position = get_field('position');
	$count = 0;
?>

<section class="c-paragraph">
	<div class="c-paragraph__withImage <?php echo $position ?>">
		<div class="c-paragraph__content">
			<?php if ($title): ?>
				<h2><?php echo $title ?></h2>
			<?php endif ?>
			<?php if ($text): ?>
				<?php echo $text ?>
			<?php endif ?>
			<?php if ($link): ?>
				<br><a class="o-button" target="<?php echo $link['target'] ?>" href="<?php echo $link['url'] ?>">
				<?php echo $link['title'] ?></a>
			<?php endif ?>
		</div>
		<?php if(get_field('images')): ?>
			<div class="c-paragraph__images js-slideshow">
				<?php while(has_sub_field('images')): 
					$image = get_sub_field('image');
					?>
					<div class="c-paragraph__image item" style="background-image: url(<?php echo $image['sizes']['medium'] ?>)"></div>
					<?php $count++ ?>
				<?php endwhile; ?>
				<?php if ($count > 1): ?>
					<div class="c-paragraph__pager js-pager"></div>
				<?php endif ?>
			</div>
		<?php endif; ?>
	</div>
</section>