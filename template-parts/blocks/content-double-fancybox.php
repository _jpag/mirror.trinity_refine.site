<?php
	/**
	 * Block Name: Paragraph with 2 popup block
	 */
    $group = get_field('group');
    $first_column = $group['first_column'];
    $second_column = $group['second_column'];
    $title_1 = $first_column['title'];
    $image_1 = $first_column['image'];
    $image_popup_1 = $first_column['image_popup'];    
    $title_2 = $second_column['title'];
    $image_2 = $second_column['image'];
    $image_popup_2 = $second_column['image_popup'];
?>

<section class="c-paragraph c-paragraph__columns">
    <div class="inner l-container">
    <div class="u-center">
            <?php if ($title_1): ?>
                <h2><?php echo $title_1 ?></h2>
            <?php endif ?>
            <?php if ($image_1): ?>
                <a data-fancybox="image_1" href="<?php echo $image_popup_1 ?>">
                    <img src="<?php echo $image_1['sizes']['medium'] ?>" alt="<?php echo $image_1['alt'] ?>">
                </a>
            <?php endif ?>
        </div>
        <div class="u-center">
            <?php if ($title_2): ?>
                <h2><?php echo $title_2 ?></h2>
            <?php endif ?>
            <?php if ($image_2): ?>
                <a data-fancybox="image_2" href="<?php echo $image_popup_2 ?>">
                    <img src="<?php echo $image_2['sizes']['medium'] ?>" alt="<?php echo $image_2['alt'] ?>">
                </a>
            <?php endif ?>
        </div>
    </div>
</section>