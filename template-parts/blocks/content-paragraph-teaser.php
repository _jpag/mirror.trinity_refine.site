<?php 
	/**
	 * Block Name: Paragraph with teaser
	 */
    $group = get_field('group');
    $paragraph = $group['paragraph'];
    $p_title = $paragraph['title'];
    $p_text = $paragraph['text'];
    $p_link = $paragraph['link'];
    $teaser = $group['teaser'];
    $t_title = $teaser['title'];
    $t_text = $teaser['text'];
    $t_link = $teaser['link'];
    $style = $teaser['style'];
?>
<section class="c-paragraph c-paragraph__columns o-background__rightSmall">
    <div class="inner l-container">
        <?php if($p_title): ?>
            <div class="c-paragraph__column">
                <h3><?php echo $p_title ?></h3>
                <?php if ($p_text): ?>
                    <p><?php echo $p_text ?></p>
                <?php endif ?>
                <?php if ($p_link): ?>
                    <a class="o-button" target="<?php echo $p_link['target'] ?>" href="<?php echo $p_link['url'] ?>">
                        <?php 
                            if ($link['title']) {
                                echo $link['title'];
                            } else {
                                echo 'more info';
                            }
                        ?>
                    </a>
                <?php endif ?>
            </div>
        <?php endif; ?>
        
        <?php if($t_title): ?>
            <div class="c-paragraph__column c-teaser <?php echo $style ?>">
                <h3><?php echo $t_title ?></h3>
                <?php if ($t_text): ?>
                    <p><?php echo $t_text ?></p>
                <?php endif ?>
                <?php if ($t_link): ?>
                    <a class="o-button" target="<?php echo $t_link['target'] ?>" href="<?php echo $t_link['url'] ?>">
                        <?php 
                            if ($t_link['title']) {
                                echo $t_link['title'];
                            } else {
                                echo 'more info';
                            }
                        ?>
                    </a>
                <?php endif ?>
            </div>
        <?php endif; ?>
    </div>
</section>
