<?php 
    $title = get_field('title');
    $columns = get_field('columns');
    $first_col = $columns['first_column'];
    $second_col = $columns['second_column'];
?>
<?php if($title): ?>
    <section class="c-paragraph c-paragraph__columns l-container">
        <?php if($title): ?>
            <h2><?php echo $title ?></h2>
        <?php endif; ?>
        <div class="inner">
            <?php if($first_col): ?>
                <div class="c-paragraph__column">
                    <?php echo $first_col ?>
                </div>
            <?php endif; ?>
            <?php if($second_col): ?>
                <div class="c-paragraph__column">
                    <?php echo $second_col ?>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>