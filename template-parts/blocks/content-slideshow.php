<?php
	/**
	 * Block Name: Hero home page
	 */
	$logo = get_field('logo');
	$text = get_field('text');
	$link = get_field('link');
	$image = get_field('image');
	$slide_1 = get_field('slide_1');
	$slide_2 = get_field('slide_2');
?>

<?php if ($image): ?>
<section class="c-slideshow">
	<div class="c-slideshow__content">
		<?php if ($logo): ?>
			<h1>
				<img src="<?php echo $logo['url'] ?>" alt="<?php echo $logo['alt'] ?>">
				<?php if ($text): ?>
					<span><?php echo $text ?></span>
				<?php endif ?>
			</h1>
		<?php endif ?>
		<?php if ($link): ?>
			<a class="o-button" target="<?php echo $link['target'] ?>" href="<?php echo $link['url'] ?>">
			<?php echo $link['title'] ?>
		</a>
		<?php endif ?>

		<div class="c-slideshow__inner">
			<?php if ($slide_1): ?>
			<a class="js-hoverHero js-anchorScroll" href="<?php echo $slide_1['link']['url'] ?>" target="<?php echo $slide_1['link']['target'] ?>" data-hover="image01">
				<h4><?php echo $slide_1['title'] ?></h4>
				<span><?php echo $slide_1['text'] ?></span>
			</a>
			<?php endif ?>

			<?php if ($slide_2): ?>
			<a class="js-hoverHero js-anchorScroll" href="<?php echo $slide_2['link']['url'] ?>" target="<?php echo $slide_2['link']['target'] ?>" data-hover="image02">
				<h4><?php echo $slide_2['title'] ?></h4>
				<span><?php echo $slide_2['text'] ?></span>
			</a>
			<?php endif ?>

		</div>
	</div>
	<div class="c-slideshow__image">
		<div class="u-cover-image">
			<img class="js-hoverHeroDefault" src="<?php echo $image['sizes']['large'] ?>" alt="<?php echo $image['alt'] ?>">
			<img id="image01" src="<?php echo $slide_1['image']['sizes']['large'] ?>" alt="<?php echo $slide_1['image']['alt'] ?>">
			<img id="image02" src="<?php echo $slide_2['image']['sizes']['large'] ?>" alt="<?php echo $slide_2['image']['alt'] ?>">
		</div>
	</div>
</section>
<?php endif ?>