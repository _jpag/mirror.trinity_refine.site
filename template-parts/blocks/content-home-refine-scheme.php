<?php 
	/**
	 * Block Name: Home Refine scheme
	 */
    $scheme = get_field('scheme');
    $scheme_mobile = get_field('scheme_mobile');
    $first_column = get_field('first_column');
    $second_column = get_field('second_column');
    $link = get_field('link');
?>

<section class="l-container c-paragraph__withScheme">
    <?php if($scheme): ?>
        <div class="scheme js-appear" js-delay="150">
            <img src="<?php echo $scheme['url'] ?>" alt="<?php echo $scheme['alt'] ?>">
        </div>        
    <?php endif; ?>

    <?php if($scheme_mobile): ?>
        <div class="scheme_mobile">
            <img src="<?php echo $scheme_mobile['url'] ?>" alt="<?php echo $scheme_mobile['alt'] ?>">
        </div>        
    <?php endif; ?>

    <?php if($first_column): ?>
        <div class="col1">
            <h3><?php echo $first_column['title'] ?></h3>
            <?php echo $first_column['text'] ?>
        </div>
    <?php endif; ?>

    <?php if($second_column): ?>
        <div class="col2">
            <h3><?php echo $second_column['title'] ?></h3>
            <?php echo $second_column['text'] ?>
        </div>
    <?php endif; ?>

    <?php if ($link): ?>
        <div class="link">
            <a class="o-button" target="<?php echo $link['target'] ?>" href="<?php echo $link['url'] ?>"><?php echo $link['title'] ?></a>
        </div>
    <?php endif ?>
</section>