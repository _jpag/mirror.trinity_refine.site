<?php 
    $title = get_field('title');
    $twitter = get_field('twitter');
?>


<section class="c-homenews">
    <div class="l-container inner">
        <div class="c-homenews__news">
        <?php 
            // args
            $args = array(
                'post_type'		=> 'post',
                'posts_per_page'=> -5,
            );

            // query
            $the_query = new WP_Query( $args );
            if( $the_query->have_posts() ): ?>
                <h3 class="c-homenews__title">
                    <?php echo $title ?>
                </h3>
                <div>
                    <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <a class="c-homenews__item" href="<?php the_permalink(); ?>">
                            <span class="category">
                            <?php foreach((get_the_category()) as $category) {
                                echo $category->name.', ';
                            } ?>
                            </span>
                            <span class="date">
                                <?php echo get_the_date('j.m.Y'); ?>
                            </span>
                            <h2>
                                <?php the_title(); ?>
                            </h2>
                        </a>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
        <?php wp_reset_query(); ?>
        </div>
        <div class="c-homenews__twitter">
            <?php if($twitter): ?>
                <?php echo $twitter ?>
            <?php endif; ?>
        </div>
    </div>
</section>
