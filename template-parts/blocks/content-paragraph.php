<?php 
    $paragraph = get_field('paragraph');
    $title = $paragraph['title'];
    $text = $paragraph['text'];
?>
<?php if($text): ?>
    <section class="c-paragraph o-background__leftSmall o-background__rightSmall">
        <div class="l-container inner">
            <?php if($title): ?>
                <h3><?php echo $title ?></h3>
            <?php endif; ?>
            <?php if($text): ?>
                <?php echo $text ?>
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>