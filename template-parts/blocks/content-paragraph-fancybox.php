<?php
	/**
	 * Block Name: Paragraph with fancybox
	 */
	$title = get_field('title');
	$text = get_field('text');
    $image = get_field('image');
    $image_popup = get_field('image_popup');
    $position = get_field('position');
    $string = preg_replace('/\s+/', '', $title)
?>

<section class="c-paragraph o-background__leftSmall o-background__rightSmall">
    <div class="c-paragraph__withImage fancybox <?php echo $position ?>">
        <div class="c-paragraph__content">
            <?php if ($title): ?>
                <h2><?php echo $title ?></h2>
            <?php endif ?>
            <?php if ($text): ?>
                <?php echo $text ?>
            <?php endif ?>
            <?php if ($image): ?>
                <br><a data-fancybox="<?php echo $string ?>_link" class="o-button" href="<?php echo $image_popup ?>">OPEN IMAGE</a>
            <?php endif ?>
        </div>
        <?php if($image): ?>
            <a data-fancybox="<?php echo $string ?>_image" href="<?php echo $image_popup ?>" class="c-paragraph__images">
                <div class="c-paragraph__image" style="background-image: url(<?php echo $image['sizes']['medium'] ?>)"></div>
            </a>
        <?php endif; ?>
    </div>
</section>