<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BrainBlank
 */
	$link = get_field('link');
	$image = get_post_thumbnail_id($post->ID);
	$size = 'medium';
	$alt = array('alt'=>get_the_title());
?>

<article id="post-<?php the_ID(); ?>" class="c-newsArticle">
	<div class="inner">
		<div class="c-newsArticle__image">
			<?php echo wp_get_attachment_image($image, $size, false, $alt) ?>
		</div>
		<div class="c-newsArticle__content">
			<div class="c-newsArticle__header">
				<a href="<?php echo get_site_url() ?>/news" class="o-button back c-pagePartner__button">BACK TO NEWSROOM</a>
				<div class="c-newsItem__info">
					<span>
						<?php
							foreach((get_the_category()) as $category){
								echo $category->name." ";
							}
						?>
					</span>
					<span>
						<?php
							echo the_time('d.m.Y', '<span>', '</span>;'); 
						?>
					</span>
				</div>
			</div>

			<?php 
				the_title( '<h1 class="entry-title">', '</h1>' );
				the_content();
			?>

			<?php if ($link): ?>
				<br><a class="o-button" target="<?php echo $link['target'] ?>" href="<?php echo $link['url'] ?>">
				<?php echo $link['title'] ?></a>
			<?php endif ?>
		</div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
