<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BrainBlank
 */
	$image = get_post_thumbnail_id($post->ID);
	$size = 'medium';
	$alt = array('alt'=>get_the_title());
?>

<a id="post-<?php the_ID(); ?>" class="c-newsItem" href="<?php echo get_permalink(); ?>">
	<div class="c-newsItem__texts">
		<div class="c-newsItem__info">
			<?php
				foreach((get_the_category()) as $category){
					echo $category->name." ";
				}
				echo the_time('d.m.Y', '<span>', '</span>;'); 
			?>
		</div>
		<?php echo the_title( '<h3 class="c-newsItem__title">', '</h3>' );?>
	</div>
	<div class="c-newsItem__image">
		<di class="u-cover-image">
			<?php echo wp_get_attachment_image($image, $size, false, $alt) ?>
		</di>
	</div>
</a><!-- #post-<?php the_ID(); ?> -->
