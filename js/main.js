(function ($) {
    var isMobile = $(window).innerWidth() < 320;

    var showreelInitializer = function () {
        var sl = $(".js-slideshow");
        if (sl.length > 0) {
            sl.cycle({
                timeout: 5000,
                speed: 500,
                slides: '.item',
                swipe: true,
                fx: 'fadeout',
                log: false,
                //swipeFx: 'scrollHorz',
                pager: '.js-pager',
                pagerTemplate: '<a href="javascript:;" class="pager"></a>',
                pagerActiveClass: 'isActive'
            });
        }
    };

    var togglerMenuMobile = function () {
        $(document).on("click", ".js-menuToggler", function () {
            $('body').toggleClass("openMenu");
        });
    };

    var anchorScroll = function() {
        var anchor = $('.js-anchorScroll');
        var header = $('.c-header').innerHeight();
        var submenu = $('.c-anchor-nav');
        if (submenu.length > 0) {
            header = header + submenu.innerHeight();
        }
        if (anchor.length > 0) {
            anchor.click(function(){
                $('html, body').animate({
                    scrollTop: $( $(this).attr('href') ).offset().top - header
                }, 500);
                return false;
            });
            // to top right away
            if ( window.location.hash ) scroll(0,0);
            // void some browsers issue
            setTimeout( function() { scroll(0,0); }, 1);

            $(function() {
                // *only* if we have anchor on the url
                if(window.location.hash) {

                    // smooth scroll to the anchor id
                    $('html, body').animate({
                        scrollTop: ($(window.location.hash).offset().top - header) + 'px'
                    }, 1000, 'swing');
                }
            });
        }
    };

    var bodyScroll = function () {
        var n = $('body');

        $(window).scroll(function (event) {
            var scroll = $(window).scrollTop();
            n.toggleClass("isScrolled", scroll > 130);
        });
    };

    var hoverHeroHome = function () {
        var btn = $('.js-hoverHero');
        btn.mouseenter(function () {
            var thisImg = $(this).data('hover');
            $('#' + thisImg).addClass('isVisible');
        });
        btn.mouseleave(function () {
            var thisImg = $(this).data('hover');
            $('#' + thisImg).removeClass('isVisible');
        });
    };

    var jsAppearInitializer = function () {
        /*
            Per attivare questa animazione basta aggiungere la classe js-appear a qualsiasi elemento.
            Se si vuole aggiungere un delay aggiungere un attributo "js-delay" es js-delay="150"
            (da utilizzare ad es. su elementi sulla stessa fila per non farli comparire tutti in blocco)
        */
        if ($(".js-appear").length > 0 && $(window).innerWidth() > 767) {
            var viewPortHeight = $(window).height();
            var currPosition = $(window).scrollTop();

            var isOnScreen = function (el) {
                // console.log('on screen');
                return ($(el).offset().top + 200) < (currPosition + viewPortHeight - ($(el).attr("js-delay") || 0));
            }
            $(".js-appear").each(function () {
                $(this).toggleClass("hidden", !isOnScreen(this));
            });
            var scrollHappened = function () {
                currPosition = $(window).scrollTop();
                $(".js-appear.hidden").each(function () {
                    $(this).toggleClass("hidden", !isOnScreen(this));
                });
            };

            $(window).scroll(scrollHappened);

            $(window).resize(function () {
                viewPortHeight = $(window).height()
                scrollHappened();
            });
        }

    };

    var carouselInitializer = function () {
        if ($(window).innerWidth() <= 767) {
            $('.js-carouselBlog').owlCarousel({
                responsive: {
                    0: {
                        items: 1,
                        margin: 20,
                        stagePadding: 30,
                        loop: true,
                        nav: false,
                        dots: true,
                        center: true,
                    }
                }
            })
        }

        $('.js-carouselHome').owlCarousel({
            loop: false,
            nav: false,
            dots: true,
            responsive: {
                0: {
                    items: 1,
                    margin: 20,
                    stagePadding: 30,
                },
                768: {
                    items: 2,
                    margin: 20,
                    stagePadding: 30,
                },
                1024: {
                    items: 3,
                    margin: 40,
                    stagePadding: 50,
                },
                1280: {
                    items: 4,
                    margin: 50,
                    stagePadding: 50,
                },
                1920: {
                    items: 5,
                    margin: 70,
                    stagePadding: 50,
                }
            }
        })
    };

    var menuMobile = function (e) {
        var el = $('.js-noPage').children('a');
        var w = $(window).innerWidth();
        if (el.length > 0 && w < 1024) {
            el.on('click', function (e) {
                e.preventDefault();
                el.siblings('.sub-menu').removeClass('isOpen');
                $(this).siblings('.sub-menu').addClass('isOpen');
            });
        }
    };

    var fancyboxInitializer = function () {
        $("[data-fancybox]").fancybox({
            buttons: [
                //"zoom",
                "share",
                //"slideShow",
                //"fullScreen",
                //"download",
                //"thumbs",
                "close"
            ],
        });
    }

    var categoriesDropdwon = function (e) {
        var el = $('#categories-dropdown');
        var getUrl = window.location;
        var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        console.log(baseUrl);
        if (el.length > 0) {
            el.change(function () {
                if ($(this).val()) {
                    window.location = baseUrl + '/news/category/' + $(this).val();
                }
            });
        }
    };

    var widgetController = function () {
        widget = $('.js-customWidget');
        close = $('.js-widgetClose');
        close.on('click', function () {
            sessionStorage.setItem('refineWidget', 'invisible');
            widget.removeClass('isVisible');
        });
        if (sessionStorage.getItem('refineWidget')) {} else {
            widget.addClass('isVisible');
        }
    };

    $(document).ready(function () {
        showreelInitializer();
        togglerMenuMobile();
        anchorScroll();
        bodyScroll();
        hoverHeroHome();
        jsAppearInitializer();
        fancyboxInitializer();
        categoriesDropdwon();
        widgetController();
        anchorScroll();
        //carouselInitializer();
        //menuMobile();
    })

})(jQuery);