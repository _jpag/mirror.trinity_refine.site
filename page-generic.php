<?php
/**
 * Template Name: Simple template
 * 
 * @package BrainBlank
 */
$image = get_post_thumbnail_id($post->ID);
$size = 'large';
$alt = array('alt'=>get_the_title());

get_header();
	while ( have_posts() ) :
		the_post();
		?>
		<article class="c-pagePartner o-background__leftSmall o-background__rightSmall">
			<div class="c-pagePartner__inner">
				<div class="c-pagePartner__text">
					<?php the_title( '<h1 class="c-page__title">', '</h1>' ); ?>
					<?php the_content(); ?>
				</div>
			</div>
		</article>
		<?php 
	endwhile; // End of the loop.
get_footer();
