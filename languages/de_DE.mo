��          �      L      �     �  =   �  /     _   D  W   �  =   �     :     O  !   ]  %        �     �     �  F   �       \   /     �  )   �  G  �  "   
  @   -  3   n  t   �  Z     C   r     �     �  !   �  /        2     :     R  S   l     �  �   �     Z	  5   i	                                                            
      	                                  Comments are closed. Continue reading<span class="screen-reader-text"> "%s"</span> Edit <span class="screen-reader-text">%s</span> It looks like nothing was found at this location. Maybe try one of the links below or a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a Comment<span class="screen-reader-text"> on %s</span> Most Used Categories Nothing Found One thought on &ldquo;%1$s&rdquo; Oops! That page can&rsquo;t be found. Pages: Posted in %1$s Proudly powered by %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Search Results for: %s Sorry, but nothing matched your search terms. Please try again with some different keywords. Tagged %1$s Try looking in the monthly archives. %1$s Project-Id-Version: _s 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/tags/_s
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-11-06 13:55+0100
Language-Team: 
X-Generator: Poedit 2.2.4
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: de
 Kommentarfunktion ist geschlossen. Weiterlesen<span class=“screen-reader-text”> “%s”</span> Edit <span class=“screen-reader-text”>%s</span> An diesem Ort wurde anscheinend nichts gefunden. Vielleicht versuchen Sie einen der folgenden Links oder eine Suche? Anscheinend können wir nicht finden, wonach Sie suchen. Vielleicht kann die Suche helfen. Leave a Comment <span class=“screen-reader-text”> auf %s</span> Meist genutzte Kategorien Nichts gefunden Ein Gedanke zu &ldquo;%1$s&rdquo; Hoppla! Diese Seite kann nicht gefunden werden. Seiten: Veröffentlicht in %1$s Stolz präsentiert von %s Bereit Deinen ersten Post zu veröffentlichen? <a href=“%1$s”>Fang hier an</a>. Suchergebnisse für: %s Es tut uns leid, aber es stimmt nichts mit Ihren Suchbegriffen überein. Bitte versuchen Sie es mit anderen Stichwörtern erneut. Markiert  %1$s Versuch in den monatlichen Archiven nachzusehen. %1$s 