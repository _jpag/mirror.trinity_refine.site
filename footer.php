<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BrainBlank
 */

?>

<footer id="colophon" class="c-footer">
	<div class="l-container">
		<div class="c-footer__inner">
			<div class="c-footer__disclaimer">
				<img src="<?php echo get_template_directory_uri() ?>/images/flag-europe.jpg" alt="European Commission - Horizon 2020">
				<?php if (!dynamic_sidebar('footerleft') ) : endif; ?>
			</div>
			<div id="form" class="c-footer__info">
				<?php if (!dynamic_sidebar('footerright') ) : endif; ?>
			</div>
		</div>

		<div class="c-footer__bottom">
			<div>
				<div class="inner">
					<p>Copyright <?php echo date("Y");?>&nbsp;©&nbsp;- REFINE</p>
					<a href="https://twitter.com/REFINE_H2020" target="_blank">
						<img class="social-icon" src="<?php echo get_template_directory_uri() ?>/images/twitter-blu.svg" alt="Facebook">
					</a>
				</div>
			</div>
			<a href="http://www.brainupstudio.it" target="_blank">
				<img src="<?php echo get_template_directory_uri() ?>/images/brainup-web-design.svg" alt="BrainUp Studio - Web design">
			</a>
		</div>
	</div>

</footer>

<?php wp_footer(); ?>

</body>

</html>