<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BrainBlank
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header id="masthead" class="c-header">
		<?php the_custom_logo(); ?>

		<nav id="site-navigation" class="main-navigation">
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
				'menu_class'     =>  'c-menu',
			) );
			// get_search_form();
			?>

		<?php if (!dynamic_sidebar('social') ) : endif; ?>
		</nav><!-- #site-navigation -->
	</header>
	<div class="c-nav__mobile js-menuToggler">
		<span class="line line--1"></span>
		<span class="line line--2"></span>
		<span class="line line--3"></span>
		<span class="line line--4"></span>
	</div>

	<?php if (!dynamic_sidebar('customarea') ) : endif; ?>