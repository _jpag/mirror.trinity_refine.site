<?php
/**
 * Template Name: Partner template
 * 
 * @package BrainBlank
 */
$image = get_post_thumbnail_id($post->ID);
$size = 'large';
$alt = array('alt'=>get_the_title());

get_header();
	while ( have_posts() ) :
		the_post();
		?>
		<article class="c-pagePartner o-background__leftSmall o-background__rightSmall">
			<div class="c-pagePartner__inner">
				<a href="<?php echo get_site_url() ?>/partners" class="o-button back c-pagePartner__button">BACK TO PARTNERS</a>
				<div class="c-pagePartner__text">
					<?php the_title( '<h1 class="c-page__title">', '</h1>' ); ?>
					<?php the_content(); ?>
				</div>
				<div class="c-pagePartner__image">
					<?php echo wp_get_attachment_image($image, $size, false, $alt); ?>
				</div>
			</div>
		</article>
		<?php 
	endwhile; // End of the loop.
get_footer();
