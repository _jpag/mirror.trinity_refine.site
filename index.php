<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BrainBlank
 */

get_header(); ?>
	<article class="c-page">
		<header class="c-page__simpleHeader">
			<div class="inner l-container c-news__filter">
				<h1 class="c-page__title">
					<?php single_post_title() ?>
				</h1>
				<div class="c-news__filter">
					<?php 
						$args = array(
							'id' => 'categories-dropdown',
							'value_field' => 'slug',
							'show_option_none' => 'Select category',
						);
						wp_dropdown_categories($args); 
					?>
				</div> 
			</div>
		</header>
		<div class="c-news">
			<div class="inner l-container">
				<?php
				if ( have_posts() ) :
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/content', 'news-item');
					endwhile;
					the_posts_navigation();

				else :
					get_template_part( 'template-parts/content', 'none' );
				endif; ?>
			</div>
		</div>
	</article>
<?php get_footer();
