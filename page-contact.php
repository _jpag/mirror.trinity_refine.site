<?php
/**
 * Template Name: Contact template
 * 
 * @package BrainBlank
 */

 $group = get_field('group');
 $map = $group['map'];
 $contact_info = $group['contact_info'];
 $address = $contact_info['address'];
 $form = $contact_info['form'];
 $contact_titles = $group['contact_titles'];
 $address_title = $contact_titles['address_title'];
 $form_title = $contact_titles['form_title'];

get_header();
	while ( have_posts() ) :
		the_post();
		?>
		<article class="c-page">
			<header class="c-page__simpleHeader">
				<div class="inner l-container">
					<?php the_title( '<h1 class="c-page__title">', '</h1>' ); ?>
				</div>
			</header>

			<div class="c-pageContact">
				<?php if($map): ?>
					<div class="c-pageContact__map">
						<div class="overlay" onClick="style.pointerEvents='none'"></div>
						<?php echo $map ?>
					</div>
				<?php endif; ?>
				<div class="c-pageContact__content l-container">
				<?php if($address): ?>
					<div class="c-pageContact__info">
						<h3><?php echo $address_title ?></h3>
						<?php echo $address ?>
					</div>
				<?php endif; ?>
				<?php if($form): ?>
					<div class="c-pageContact__form">
						<h3><?php echo $form_title ?></h3>
						<?php echo $form ?>
					</div>
				<?php endif; ?>
				</div>
			</div>
		</article>
		<?php 
	endwhile; // End of the loop.
get_footer();
